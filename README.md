# Can-Sat 2024

## Todo Can-Sat:
- [x] i2c auf raspi
- [x] Luftdruck und Temperatur-Sensor
- [x] Daten in Datei speichern mit Zeitstempel
- [x] UART mappen
- [ ] Übertragung per Modul testen
- [x] Alles in Autostart
 
## Todo Bodenstation:
- [ ] Succesfull connected notification
- [ ] Werte empfangen
