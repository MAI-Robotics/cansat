import serial
import time

ser = serial.Serial(
    port='/dev/ttyS0',  
    baudrate=115200,       
    timeout=1           
)

def sendCommand(Command):
    data_to_send = Command
    ser.write(data_to_send)
    response = ser.readline()
    return response

def sendData(temperature, pressure):
    data_to_send = [temperature, pressure]
    send_command = b'\xC2' + len(data_to_send).to_bytes(1, 'big') + data_to_send
    ser.write(send_command)
