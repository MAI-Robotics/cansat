import RPi.GPIO as GPIO
import time

buzzer_pin = 16

def initBuzzer():
     GPIO.setmode(GPIO.BCM)
     GPIO.setup(buzzer_pin, GPIO.OUT)
     GPIO.output(buzzer_pin, GPIO.HIGH)
     GPIO.cleanup()

def buzzerAn():
     GPIO.output(buzzer_pin, GPIO.HIGH)

def buzzerAus():
     GPIO.output(buzzer_pin, GPIO.LOW)

def ausschalten():
     GPIO.cleanup()

def buzzerStarted():
     buzzerAn()
     time.sleep(1)
     buzzerAus()
     ausschalten()

def buzzerSucces():
     buzzerAn()
     time.sleep(1)
     buzzerAus()
     time.sleep(1)
     buzzerAn()
     time.sleep(1)
     buzzerAus()
     ausschalten()

def buzzerError():
     buzzerAn()
     while True:
          time.sleep(1)
     ausschalten()