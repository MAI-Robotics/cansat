from bmp280 import BMP280
import datetime
import time
try:
    from smbus2 import SMBus
except ImportError:
    from smbus import SMBus

path_to_txt = "/home/cansat/cansat/Can-Sat/data/data.txt"

bmp = BMP280(i2c_dev=1, i2c_addr=0x76)

# Initialise the BMP280
bus = SMBus(1)
bmp = BMP280(i2c_dev=bus)

def getPressure():
    return bmp.get_pressure()

def getTemperature():
    return bmp.get_temperature()

def saveData(temperature, pressure):
    f = open(path_to_txt, "a")
    ct = datetime.datetime.now()
    f.write(f"Temperatur: {temperature}, Luftdruck: {pressure}, Timestamp: {ct}\n")
    f.close()

    f = open(path_to_txt, "r")
    print(f.read())
