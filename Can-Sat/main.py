#start: ground pressure messen, lora testen
#Warte bis in luft: current pressure < ground pressure
#Warte bis gelandet current pressure = ground presuer +-differanz
#Buzzer aktivieren


#START des scripts
import sensor
import time


def main():
    while (True) :
        temperature = sensor.getTemperature()
        pressure = sensor.getPressure()
        sensor.saveData(temperature, pressure)
        time.sleep(1)

if __name__ == "__main__":
    main()
